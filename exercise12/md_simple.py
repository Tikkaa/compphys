"""
Simple Molecular Dynamics code for course FYS-4096 Computational Physics

Problem 1:
- Make the code to work and solve H2 using the Morse potential.
- Modify and comment especially at parts where it reads "# ADD"
- Follow instructions on ex12.pdf

Problems 2:
- Add observables: temperature, distance, and heat capacity.
- Follow instructions on ex12.pdf

Problem 3:
- Add Lennard-Jones capability etc.
- Follow instructions on ex12.pdf

Problem 4:
- Temperature dependent data and analysis
- Follow instructions on ex12.pdf

"""




from numpy import *
from matplotlib.pyplot import *

class Atom:
    def __init__(self,index,mass,dt,dims):
        self.index = index
        self.mass = mass
        self.dt = dt
        self.dims = dims
        self.LJ_epsilon = None
        self.LJ_sigma = None
        self.R = None
        self.v = None
        self.force = None

    def set_LJ_parameters(self,LJ_epsilon,LJ_sigma):
        self.LJ_epsilon = LJ_epsilon
        self.LJ_sigma = LJ_sigma

    def set_position(self,coordinates):
        self.R = coordinates
        
    def set_velocity(self,velocity):
        self.v = velocity

    def set_force(self,force):
        self.force = force

class Observables:
    def __init__(self):
        self.E_kin = []
        self.E_pot = []
        self.distance = []
        self.Temperature = []

def calculate_energetics(atoms, type):
    N = len(atoms)
    V = 0.0
    E_kin = 0.0
    
    # Potential energy of the system:
    for i in range(N):
        for j in range(i+1,N):
            if i != j:
                V += pair_potential(atoms[i], atoms[j], type)
    
    # Kinetic energy of the system:
    for i in range(N):
        E_kin += 1/2*atoms[i].mass*linalg.norm(atoms[i].v)**2
    
    return E_kin, V

def calculate_force(atoms, type):
    # Calculating the forces felt by the atoms. Returns the in an array of
    # vectors where the vector i corresponds to atom i.
    N = len(atoms)
    ij_map = zeros((N,N),dtype=int)
    Fs = []
    ind = 0
    for i in range(0,N-1):
        for j in range(i+1,N):
            Fs.append(pair_force(atoms[i],atoms[j], type))
            ij_map[i,j] = ind
            ij_map[j,i] = ind
            ind += 1
    F = []
    for i in range(N):
        f = zeros(shape=shape(atoms[i].R))
        for j in range(N):
            ind = ij_map[i,j]
            if i<j:
                f += Fs[ind]
            elif i>j:
                f -= Fs[ind]
        F.append(f)
    F = array(F)
    return F

def pair_force(atom1,atom2,type):
    if type == "Morse":
        return Morse_force(atom1,atom2)
    else:
        return lennard_jones_force(atom1, atom2)

def pair_potential(atom1,atom2,type):
    if type == "Morse":
        return Morse_potential(atom1,atom2)
    else:
        return lennard_jones_potential(atom1, atom2)

def Morse_potential(atom1,atom2):
    # H2 parameters given here
    De = 0.1745
    re = 1.40
    a = 1.0282
    r = atom1.R-atom2.R
    dr = sqrt(sum(r**2))
    # Calculating morse potential. Subtracting De to make the potential 0 when
    # atoms are infinitely far apart.
    V_morse = De*(1-exp(-a*(dr-re)))**2 - De
    return V_morse

def Morse_force(atom1,atom2):
    # H2 parameters
    De = 0.1745
    re = 1.40
    a = 1.0282
    r = atom1.R-atom2.R
    dr = sqrt(sum(r**2))
    
    # F = -gradient(V) for conservative fields.
    F = -2*De*a*exp(-a*(dr-re))*(1-exp(-a*(dr-re)))

    # Adding the directionality
    return F/linalg.norm(r)*r

def lennard_jones_potential(atom1,atom2):
    epsilon = sqrt(atom1.LJ_epsilon*atom2.LJ_epsilon)
    sigma = (atom1.LJ_sigma+atom2.LJ_sigma)/2
    
    # The location of the minimum of lj-potential is the root of its derivative.
    # du/dr = 4*epsilon*(6s^6/r^7 - 12s^12/r^13) = 0
    # r^6 = 2*s^6
    # r = 2^1/6*s
    
    # The energy at this r:
    # 4e*((2^-1/6)^12 - (2^-1/6)^6) = 4e*(1/4 - 1/2) = 4e*(-1/4) = -e
    
    r = linalg.norm(atom1.R - atom2.R)
    
    u = 4*epsilon*((sigma/r)**12-(sigma/r)**6)
    
    return u

def lennard_jones_force(atom1,atom2):
    epsilon = sqrt(atom1.LJ_epsilon*atom2.LJ_epsilon)
    sigma = (atom1.LJ_sigma+atom2.LJ_sigma)/2
    r = linalg.norm(atom1.R - atom2.R)
    
    # Calculating the magnitude of the force:
    F = 4*epsilon*(6*(sigma**6/(r**7))-12*(sigma**12/(r**13)))
    
    # Adding directionality:
    return -F/r*(atom1.R - atom2.R)

def velocity_verlet_update(atoms, type):
    # Calculating new values for atom positions and velocities dt from 
    # current simualted state.
    
    dt = atoms[0].dt
    dt2 = dt**2
    # Updating positions.
    for i in range(len(atoms)):
        atoms[i].R += dt*atoms[i].v + dt2/(2*atoms[i].mass)*atoms[i].force
    Fnew = calculate_force(atoms, type)
    # Updating velocities.
    for i in range(len(atoms)):
        atoms[i].v += dt/(2*atoms[i].mass)*(Fnew[i] + atoms[i].force)
        atoms[i].force = Fnew[i] # update force
    return atoms
    
def initialize_positions(atoms):
    # diatomic case
    atoms[0].set_position(array([-0.8,0.0,0.0]))
    atoms[1].set_position(array([0.7,0.0,0.0]))

def initialize_velocities(atoms):
    # ADD --> in problem 4: comment on what is v_max
    # diatomic case
    dims = atoms[0].dims
    kB=3.16e-6 # in hartree/Kelvin
    for i in range(len(atoms)):
        v_max = sqrt(3.0/atoms[i].mass*kB*10.0)
        atoms[i].set_velocity(array([1.0,0.0,0.0])*v_max)
    atoms[1].v = -1.0*atoms[0].v

def initialize_force(atoms, type):
    F=calculate_force(atoms, type)
    for i in range(len(atoms)):
        atoms[i].set_force(F[i])

def Temperature(E_k, dims, N_atoms):
    # Boltzmann constant in Hartree/Kelvin
    kB = 3.16e-6
    
    return 2*E_k/(dims*N_atoms*kB)

def calculate_observables(atoms,observables,dims, N_atoms, type):
    # Energetics:
    E_k, E_p = calculate_energetics(atoms, type)
    observables.E_kin.append(E_k)
    observables.E_pot.append(E_p)
    
    #Temperature:
    observables.Temperature.append(Temperature(E_k, dims, N_atoms))

    # Distances:
    n = len(atoms)
    distances = 0
    count = 0
    for i in range(n):
        for j in range(i,n):
            if i != j:
                distances += linalg.norm(atoms[i].R - atoms[j].R)
                count += 1
    
    observables.distance.append(distances/count)
    
    return observables

def main():
    N_atoms = 2
    dims = 3
    dt = 0.1
    mass = 1860.0

    print("Using Morse potential:")
    type = "Morse"

    # Initialize atoms
    atoms = []    
    for i in range(N_atoms):
        atoms.append(Atom(i,mass,dt,dims))
        atoms[i].set_LJ_parameters(0.1745,1.25)
    # Initialize observables
    observables = Observables()

    # Initialize positions, velocities, and forces
    initialize_positions(atoms)
    initialize_velocities(atoms)
    initialize_force(atoms, type)

    for i in range(100000):
        atoms = velocity_verlet_update(atoms, type)
        if ( i % 10 == 0):
            observables = calculate_observables(atoms,observables,dims, N_atoms, type)            

    # Print energies
    E_kin = array(observables.E_kin)
    E_pot = array(observables.E_pot)
    print('E_kin',mean(E_kin))
    print('E_pot',mean(E_pot))
    print('E_tot',mean(E_kin)+mean(E_pot))
    subplot(311)
    plot(E_kin + E_pot)
    title("Kinetic energy")
    subplot(312)
    plot(array(observables.Temperature))
    title("Temperature")
    subplot(313)
    plot(array(observables.distance))
    title("Average interatomic distance")
    show()
    print("Standard deviation of temperature:")
    print(std(array(observables.Temperature)))
    
    print()
    print("Using Lennard-Jones potential:")
    type = "LJ"
    
    # Initialize atoms
    atoms = []    
    for i in range(N_atoms):
        atoms.append(Atom(i,mass,dt,dims))
        # 0.1745 from potential minimum, 1.25 from 2^(-1/6)*interatomic
        # distance at the minimum.
        atoms[i].set_LJ_parameters(0.1745,1.25)
    # Initialize observables
    observables = Observables()

    # Initialize positions, velocities, and forces
    initialize_positions(atoms)
    initialize_velocities(atoms)
    initialize_force(atoms, type)

    for i in range(100000):
        atoms = velocity_verlet_update(atoms, type)
        if ( i % 10 == 0):
            observables = calculate_observables(atoms,observables,dims, N_atoms, type)            

    # Print energies
    figure()
    E_kin = array(observables.E_kin)
    E_pot = array(observables.E_pot)
    print('E_kin',mean(E_kin))
    print('E_pot',mean(E_pot))
    print('E_tot',mean(E_kin)+mean(E_pot))
    subplot(311)
    plot(E_kin + E_pot)
    title("Kinetic energy")
    subplot(312)
    plot(array(observables.Temperature))
    title("Temperature")
    subplot(313)
    plot(array(observables.distance))
    title("Average interatomic distance")
    show()
    print("Standard deviation of temperature:")
    print(std(array(observables.Temperature)))
    
    
    figure()
    R = linspace(1.2,2,100)
    V = 0*R
    epsilon = 0.1745
    sigma = 1.25
    for i in range(len(R)):
        V[i] = 4*epsilon*((sigma/R[i])**12 -(sigma/R[i])**6)
    plot(R,V)

if __name__=="__main__":
    main()
        
