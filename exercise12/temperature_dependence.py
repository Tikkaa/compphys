from md_simple import *
from numpy import *
from matplotlib.pyplot import *


def main():
    T = linspace(100,4000,10)
    kB = 3.16e-6
    
    
    # T = 2*E_k/(dims*N_atoms*kB)
    E_k = 3*kB*T
    
    N_atoms = 2
    dims = 3
    dt = 0.5
    mass = 1860.0

    type = "Morse"

    # Initialize atoms
    atoms = []    
    for i in range(N_atoms):
        atoms.append(Atom(i,mass,dt,dims))
        atoms[i].set_LJ_parameters(0.1745,1.25)
    
    # E_k = 1/2*m*v^2
    v= zeros((10,3))
    for i in range(len(T)):
        v[i] = [sqrt(2/atoms[0].mass*E_k[i]), 0, 0]
    
    dist = 0*T
    E_pot = 0*T
    E_kin_c = 0*T
    
    for i in range(len(T)):
        # Initialize observables
        observables = Observables()
        
        # Initialize positions, velocities, and forces
        initialize_positions(atoms)
        atoms[0].set_position(array([0,0.0,0.0]))
        atoms[1].set_position(array([1.4,0.0,0.0]))
        initialize_velocities(atoms)
        atoms[0].set_velocity(array(v[i]))
        atoms[1].set_velocity(array(-1*v[i]))
        initialize_force(atoms, type)

        for j in range(1000):
            atoms = velocity_verlet_update(atoms, type)
            if ( j % 10 == 0):
                observables = calculate_observables(atoms,observables,dims, N_atoms, type)
                
        dist[i] = mean(observables.distance)
        E_pot[i] = mean(observables.E_pot)
        E_kin_c[i] = mean(observables.E_kin)
    
    # Heat capacity: dE/dT
    heat_capacity = gradient(E_kin_c + E_pot, T[1]-T[0])
    
    figure()
    subplot(411)
    plot(T, dist)
    title("interatomic distance")
    xlabel("Temperature")
    subplot(412)
    plot(T,E_pot)
    title("Potential energy")
    xlabel("Temperature")
    subplot(413)
    plot(T,E_k)
    plot(T,E_kin_c)
    title("Kinetic energy")
    xlabel("Temperature")
    subplot(414)
    plot(T,heat_capacity)
    title("Heat capacity")
    xlabel("Temperature")
    
main()