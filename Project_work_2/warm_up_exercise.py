"""
Computational physics
Project work 2
Warm up exercise
"""

import h5py
import numpy as np
from scipy.interpolate import interp2d
import matplotlib.pyplot as plt

# Reading the file. It contains data, x_grid and y_grid.
file = h5py.File("ProjectWork2_warm_up_data.h5", "r")

# Getting the arrays from the read file. The transposition is done to make
# the data fit the coordinates.
data = np.transpose(np.array(file.get("data")))
x_grid = np.array(file.get("x_grid"))
y_grid = np.array(file.get("y_grid"))

file.close()

# Defining the grid.
X, Y = np.meshgrid(x_grid, y_grid)

# Plotting the data.
plt.contourf(X, Y, data)
plt.xlabel('x')
plt.ylabel('y')
plt.show()

# Interpolating the data.
interpolation = interp2d(Y, X, data, kind='cubic')

# Testing an known point f(0.1, 0.1) = 0.1618:
test_value = interpolation(0.1, 0.1)[0]
real_value = 0.1618
diff = abs(test_value - real_value)

# Testing the function and printing out some information.
print("Testing the interpolation at (0.1, 0,1):")
print()
print("Interpolated value:", test_value)
print("Real value:", real_value)
print("Difference:", diff)
print()
if diff > 0.01:
    print("Something is wrong with the interpolation!")


# Defining the points on the line that is of interest.
x_line = np.linspace(-1.5, 0.25, 100)
y_line = np.linspace(-1.5, 1.02, 100)

# The points' lenght along the line.
points_co = np.zeros_like(x_line)
for i in range(len(points_co)):
    points_co[i] = np.sqrt((x_line[0] - x_line[i])**2 + (y_line[0] - y_line[i])**2)

# Calculating the values on the line from the interpolation.
points = np.zeros_like(points_co)
for i in range(len(points)):
    points[i] = interpolation(x_line[i], y_line[i])

# Plotting the ponits on the line.
plt.figure()
plt.plot(points_co, points)
plt.xlabel("Lentgh along the line")
plt.ylabel("Interpolated value")

# Saving the data from the line.
save_file = h5py.File("warm_up_interpolated.h5", "w")
save_file.create_dataset("values", data=points)
save_file.create_dataset("x_coordinates", data=x_line)
save_file.create_dataset("y_coordinates", data=y_line)

save_file.close()
