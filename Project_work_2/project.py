"""
Computational physics project 2.
Problem 5: Ising model of a ferromagnet.
"""

import numpy as np
import random
import matplotlib.pyplot as plt

def makegrid(gridsize):
    # Makes the grid of dipoles. They have the value of 1 or -1, determined
    # randomly for each grid point. Returns the grid.
    
    grid = np.zeros((gridsize,gridsize))
    for i in range(gridsize):
        for j in range(gridsize):
            if random.random() > 0.5:
                grid[i,j] = 1
            else:
                grid[i,j] = -1
                
    return grid

def dipole_energy(i, j, grid, mode, h=0):
    # Calculates the energy of a dipole in grid at coordinates (i,j). If mode
    # is 'nearest', the energy will be calculated using the four (or fewer in
    # the case the point is at an edge) dipoles that are on each side of the
    # point. If it is 'next', also points that are diagonally adjacent will
    # be used.
    
    # Interaction energy constants.
    # Directly adjacent:
    J1 = 4
    # Diagonally adjacent:
    J2 = 1
    
    max_ind = grid.shape[0] - 1
    spin = grid[i,j]
    E = 0
    
    # Conditional stucture to take the case where (i,j) is at the edge of the
    # grid and does not have the full number of adjacent dipoles.
    if i > 0:
        E += -J1*spin*grid[i-1,j]
    if i < max_ind:
        E += -J1*spin*grid[i+1,j]
    if j > 0:
        E += -J1*spin*grid[i,j-1]
    if j < max_ind:
        E += -J1*spin*grid[i,j+1]
        
    # For diagonally adjacent dipoles.
    if mode == 'next':
        if i > 0 and j > 0:
            E += -J2*spin*grid[i-1,j-1]
        if i > 0 and j < max_ind:
            E += -J2*spin*grid[i-1,j+1]
        if i > max_ind and j > 0:
            E += -J2*spin*grid[i+1,j-1]
        if i < max_ind and j < max_ind:
            E += -J2*spin*grid[i+1,j+1]
    # External magnetic field contribution
    E += -h*grid[i,j]
    
    return E

def total_energy(grid, mode, h=0):
    
    gridsize = grid.shape[0]
    
    E = 0
    
    for i in range(gridsize):
        for j in range (gridsize):
            E += dipole_energy(i, j, grid, mode,h)

    # Every interaction is counted twice, so the result must be halved.
    return 1/2*E

def monte_carlo_move(grid, mode, T, h=0):
    # Randomly chooses a dipole in the grid and has a chance to flip it based
    # on energy calculations.
    
    max_ind = grid.shape[0] - 1
    
    # Choosing point.
    i = random.randint(0,max_ind)
    j = random.randint(0,max_ind)
    
    # Dipole energy.
    E = dipole_energy(i,j,grid,mode,h)
    
    # Flip the dipole if the energy is above 0 or randomly based on temperature
    # and energy.
    if np.exp(E/T) > random.random():
        grid[i,j] *= -1

def main():
    
    
    # Number of simulations per temperature:
    sims = 40
    
    temperatures = np.linspace(1, 10, 10)
    
    gridsize = 10
    
    # Number of iterations per simulation.
    N = 100*gridsize**2
    
    # Nearest:
    magnetizations = []
    heat_capacities = []
    energies = []
    susceptibilities = []
    
    print("Temperature dependence calculation progress:")
    progress = 0
    print(progress/len(temperatures)*100, '%')
    
    # Doing the simulations in different temperatures.
    for T in temperatures:
        mag = 0
        mag_h = 0
        dh = 1
        E_tot = np.zeros(sims)
        for sim in range(sims):
            grid = makegrid(gridsize)
            for i in range(N):
                monte_carlo_move(grid, 'nearest', T)
            mag += abs(sum(sum(grid)))/gridsize**2
            E_tot[sim] = total_energy(grid, 'nearest')
        for sim in range(sims):
            grid = makegrid(gridsize)
            for i in range(N):
                monte_carlo_move(grid, 'nearest', T, dh)
            mag_h += abs(sum(sum(grid)))/gridsize**2
        # Saving the simulation results in the corresponding list.
        magnetizations.append(mag/sims)
        heat_capacities.append(np.var(E_tot)**2/T**2)
        energies.append(np.mean(E_tot)/gridsize**2)
        susceptibilities.append((mag_h-mag)/dh)
        
        progress += 1
        print(progress/len(temperatures)*100/2, '%')
        
        
    # Next nearest:
    magnetizations_next = []
    heat_capacities_next = []
    energies_next = []
    susceptibilities_next = []
    
    # Doing the simulations in different temperatures.
    for T in temperatures:
        mag = 0
        mag_h = 0
        dh = 1
        E_tot = np.zeros(sims)
        for sim in range(sims):
            grid = makegrid(gridsize)
            for i in range(N):
                monte_carlo_move(grid, 'next', T)
            mag += abs(sum(sum(grid)))/gridsize**2
            E_tot[sim] = total_energy(grid, 'next')
        for sim in range(sims):
            grid = makegrid(gridsize)
            for i in range(N):
                monte_carlo_move(grid, 'next', T, dh)
            mag_h += abs(sum(sum(grid)))/gridsize**2
        # Saving the simulation results in the corresponding list.
        magnetizations_next.append(mag/sims)
        heat_capacities_next.append(np.var(E_tot)**2/T**2)
        energies_next.append(np.mean(E_tot)/gridsize**2)
        susceptibilities_next.append((mag_h-mag)/dh)
        
        progress += 1
        print(progress/len(temperatures)*100/2, '%')
    
    plt.figure()
    plt.subplot(411)
    plt.plot(temperatures, magnetizations)
    plt.plot(temperatures, magnetizations_next)
    plt.title("Magnetization")
    plt.legend(['Nearest', 'Next nearest'])
    plt.subplot(412)
    plt.plot(temperatures, heat_capacities)
    plt.plot(temperatures, heat_capacities_next)
    plt.title("Heat capacity")
    plt.subplot(413)
    plt.plot(temperatures, energies)
    plt.plot(temperatures, energies_next)
    plt.title("Total energy")
    plt.subplot(414)
    plt.plot(temperatures, susceptibilities)
    plt.plot(temperatures, susceptibilities_next)
    plt.title("Magnetic susceptibility")
    plt.xlabel("Temperature (K)")
    
    # Calculating size dependecies.
    
    print("Size dependence calculation progress:")
    progress = 0
    
    gridsize = [4, 8, 16, 32]
    T = 4
    
    magnetizations_size = []
    heat_capacities_size = []
    energies_size = []
    susceptibilities_size = []
    
    for size in gridsize:
        
        N = 100*size**2
        
        mag = 0
        mag_h = 0
        dh = 1
        E_tot = np.zeros(sims)
        for sim in range(sims):
            grid = makegrid(size)
            for i in range(N):
                monte_carlo_move(grid, 'next', T)
            mag += abs(sum(sum(grid)))/size**2
            E_tot[sim] = total_energy(grid, 'next')
        # Saving the simulation results in the corresponding list.
        magnetizations_size.append(mag/sims)
        heat_capacities_size.append(np.var(E_tot)**2/T**2)
        energies_size.append(np.mean(E_tot)/size**2)
        
        progress += 1
        print(progress/len(gridsize)*100, "%")
        
    plt.figure()
    plt.plot(gridsize, energies_size)
    plt.title("Average energy per spin")
    plt.xlabel("Grid side length")
main()