"""
Hartree code for N-electron 1D harmonic quantum dot

- Related to FYS-4096 Computational Physics 
- Test case using simpson integrations should give:

    Total energy      11.502873299221452
    Kinetic energy    3.622113606711247
    Potential energy  7.880759692510205

    Density integral  3.9999999999999996

- Job description in short (more details in the pdf-file): 
  -- Problem 1: add/fill needed functions and details
  -- Problem 2: Include input and output as text file
  -- Problem 3: Include input and output as HDF5 file 
"""



from numpy import *
from matplotlib.pyplot import *
import scipy.sparse as sp
import scipy.sparse.linalg as sla
from scipy.integrate import simps
import h5py
import os

def hartree_potential(ns,x):
    """ 
    Hartree potential using Simpson integration. Takes in the electron density
    and the points it is defined in. Returns the calculted potential in those
    points.
    """
    Vhartree=0.0*ns
    for ix in range(len(x)):
        r = x[ix]
        f = 0.0*x
        for ix2 in range(len(x)):
            rp = x[ix2]
            f[ix2]=ns[ix2]*ee_potential(r-rp)
        Vhartree[ix]=simps(f, x)
    return Vhartree

def ee_potential(x):
    global ee_coef
    """ 1D electron-electron interaction """
    return ee_coef[0]/(sqrt(x**2 + ee_coef[1]))

def ext_potential(x,m=1.0,omega=1.0):
    """ 1D harmonic quantum dot """
    return 0.5*m*omega**2*x**2

def density(psis):
    ns=zeros((len(psis[0]),))
    for i in range(len(ns)):
        ns[i] = np.abs(psis[0][i]**2 + psis[1][i]**2 + psis[2][i]**2 + psis[3][i]**2)
    return ns
    
def initialize_density(x,dx,normalization=1):
    rho=exp(-x**2)
    A=simps(rho,x)
    return normalization/A*rho

def check_convergence(Vold,Vnew,threshold):
    difference_ = amax(abs(Vold-Vnew))
    print('  Convergence check:', difference_)
    converged=False
    if difference_ <threshold:
        converged=True
    return converged

def diagonal_energy(T,orbitals,x):
    """ 
    Calculate diagonal energy
    (using Simpson)
    """
    Tt=sp.csr_matrix(T)
    E_diag=0.0
    
    for i in range(len(orbitals)):
        evec=orbitals[i]
        E_diag+=simps(evec.conj()*Tt.dot(evec),x)
    return E_diag

def offdiag_potential_energy(orbitals,x):
    """ 
    Calculate off-diagonal energy
    (using Simpson)
    """
    U = 0.0
    for i in range(len(orbitals)-1):
        for j in range(i+1,len(orbitals)):
            fi = 0.0*x
            for i1 in range(len(x)):
                fj = 0.0*x
                for j1 in range(len(x)):
                    fj[j1]=abs(orbitals[i][i1])**2*abs(orbitals[j][j1])**2*ee_potential(x[i1]-x[j1])
                fi[i1]=simps(fj,x)
            U+=simps(fi,x)
    return U

def save_ns_in_ascii(ns,filename):
    s=shape(ns)
    f=open(filename+'.txt','w')
    for ix in range(s[0]):
        f.write('{0:12.8f}\n'.format(ns[ix]))
    f.close()
    f=open(filename+'_shape.txt','w')
    f.write('{0:5}'.format(s[0]))
    f.close()
    
def load_ns_from_ascii(filename):
    f=open(filename+'_shape.txt','r')
    for line in f:
        s=array(line.split(),dtype=int)
    f.close()
    ns=zeros((s[0],))
    d=loadtxt(filename+'.txt')
    k=0
    for ix in range(s[0]):
        ns[ix]=d[k]
        k+=1
    return ns

def save_orbitals_in_ascii(orbitals,filename):
    s=shape(orbitals)
    f=open(filename+'.txt','w')
    for ix in range(s[0]):
        for ix2 in range(s[1]):
            f.write('{0:12.8f} '.format(orbitals[ix][ix2]))
        f.write('\n')
    f.close()
    f=open(filename+'_shape.txt','w')
    f.write('{0:5} {1:5}'.format(s[0],s[1]))
    f.close()

def load_orbitals_from_ascii(filename):
    f=open(filename+'_shape.txt','r')
    for line in f:
        s=array(line.split(),dtype=int)
    f.close()
    orbitals=zeros((s[0],s[1]))
    f = open(filename + '.txt', 'r')
    i = 0
    for line in f:
        orbitals[i] = array(line.split(), dtype=float)
        i += 1
    f.close()
    return orbitals

def save_misc_in_ascii(E_tot, E_kin, E_pot, x, filename):
    # Function that saves the rest of the data for storage purposes.
    
    f = open(filename+'.txt', 'w')
    
    f.write("\n".join((
            r'Total energy:%.2f' % (E_tot, ),
            r'Kinetic energy:%.2f' % (E_kin, ),
            r'Potential energy:%.2f' % (E_pot, ))))
    
    f.write("\n")
    for i in range(len(x)):
        f.write(str(x[i]))
        f.write(", ")

def save_data_to_hdf5_file(fname,orbitals,density,N_e,occ,grid,ee_coefs):
    # Function to save the data in HDF5 format.
    f = h5py.File(fname,"w")
    
    oset = f.create_dataset("orbitals",data=orbitals,dtype='f')
    oset.attrs["info"] = '1D orbitals as (len(grid),N_electrons)'
    
    gset = f.create_dataset("grid",data=grid,dtype='f')
    gset.attrs["info"] = '1D grid'

    dset = f.create_dataset("density", data=density, dtype='f')
    dset.attrs["info"] = 'electron density'
    
    nset = f.create_dataset("N_e", data=N_e, dtype='f')
    nset.attrs["info"] = 'number of electrons'
    
    ocset = f.create_dataset("occ", data=occ, dtype='i')
    ocset.attrs["info"] = 'occupation of the orbitals'
    
    eset = f.create_dataset("ee_coefs", data=ee_coefs, dtype='f')
    eset.attrs["info"] = 'e-e potential parameters'
    
    f.close()
    return

def read_hdf5_file(fname):
    # Function to read the data from HDF5 files.
    f = h5py.File(fname,"r")
    print('Keys in hdf5 file: ',list(f.keys()))
    x = array(f["grid"])
    ns = array(f["density"])
    ee_coefs = array(f["ee_coefs"])
    occ = array(f["occ"])
    orbs = array(f["orbitals"])
    f.close()
    return x, ns, ee_coefs, occ, orbs

def calculate_SIC(orbitals,x):
    V_SIC = []
    for i in range(len(orbitals)):
        V_SIC.append(-hartree_potential(abs(orbitals[i])**2,x))
    return V_SIC

def same_spin(occ, i, j):
    # Checks if the two electrons have the same spin, and returns the
    # corresponding truth value
    same = True
    for ix in range(len(occ)):
        if ix != occ[ix]:
            if abs(i-j) % 2 != 0:
                same = False
    return same

def exchange_potential(ns,x, psis, N_e, occ):
    """
    calculate the exchange potential for the Hartree Fock approximation.
    """
    Vx = []
    for i in range(N_e):
        Vxx = 0.0 * ns
        for ix in range(len(x)):
            r = x[ix]
            f = 0.0 * x
            nhf = 0.0 * ns
            for ix2 in range(len(x)):
                rp = x[ix2]
                # Calculating nhf.
                for j in range(N_e):
                    if same_spin(occ, i, j):
                        part_of_sum = np.conj(psis[j][ix2])*psis[i][ix2]
                    else:
                        part_of_sum = 0
                    nhf[ix] = nhf[ix] + part_of_sum
                f[ix2] = nhf[ix2] * ee_potential(r - rp)
            Vxx[ix] = -1*simps(f, x)
        Vx.append(Vxx)
    return Vx
            
def normalize_orbital(evec,x):
    """ Normalize orbitals to one """
    return evec/sqrt(simps(evec**2, x)) 
 
def kinetic_hamiltonian(x):
    grid_size = x.shape[0]
    dx = x[1] - x[0]
    dx2 = dx**2
    
    H0 = sp.diags(
        [
            -0.5 / dx2 * np.ones(grid_size - 1),
            1.0 / dx2 * np.ones(grid_size),
            -0.5 / dx2 * np.ones(grid_size - 1)
        ],
        [-1, 0, 1])
    return H0

def main():

     # --- Setting up the system etc. ---
    global ee_coef
    # e-e potential parameters [strenght, smoothness]
    ee_coef = [1.0, 1.0]

    # 1D occupations each orbital can have max 2, i.e., spin up and spin down
    # e.g., occ = [0,0,1,1] means lowest up and down, next lowest up and down
    #       occ = [0,1,2,3] means all spin up
    occ = [0,1,2,3]
    N_e = len(occ)
    
    # grid
    x=linspace(-4,4,120)

    # threshold
    threshold=1.0e-4

    # mixing value
    mix=0.2

    # maximum number of iterations
    maxiters = 100
    # --- End setting up the system etc. ---



    # Initialization and calculations start here
    dx = x[1]-x[0]
    T = kinetic_hamiltonian(x)
    Vext = ext_potential(x)

    
    if os.path.isfile('results'):
        x, ns, ee_coefs, occ, orbitals = read_hdf5_file('results')
        VSIC = calculate_SIC(orbitals,x)
    else:
        if os.path.isfile('density.txt'):
            ns=load_ns_from_ascii('density')
        else:
                ns=initialize_density(x,dx,N_e)
        
        if os.path.isfile('orbitals.txt'):
            orbitals=load_orbitals_from_ascii('orbitals')
            VSIC = calculate_SIC(orbitals,x)
        else:
            VSIC=[]
            for i in range(N_e):
                VSIC.append(ns*0.0)
        
        
    print('Density integral        ', simps(ns,x))
    print(' -- should be close to  ', N_e)
    
    print('\nCalculating initial state')
    Vhartree=hartree_potential(ns,x)

    Veff=sp.diags(Vext+Vhartree,0)
    H=T+Veff
    # Iterating until convergence or the maximum number iterations is reached.
    # Hartree:
    """
    for i in range(maxiters):
        print('\n\nIteration #{0}'.format(i))
        orbitals=[]
        for i in range(N_e):
            print('  Calculating orbitals for electron ', i+1)
            eigs, evecs = sla.eigs(H+sp.diags(VSIC[i],0), k=N_e, which='SR')
            eigs=real(eigs)
            evecs=real(evecs)
            print('    eigenvalues', eigs)
            evecs[:,occ[i]]=normalize_orbital(evecs[:,occ[i]],x)
            orbitals.append(evecs[:,occ[i]])
        Veff_old = 1.0*Veff
        ns=density(orbitals)
        Vhartree=hartree_potential(ns,x)
        VSIC=calculate_SIC(orbitals,x)
        Veff_new=sp.diags(Vext+Vhartree,0)
        if check_convergence(Veff_old,Veff_new,threshold):
            break
        else:
            #Mixing the potential 
            Veff= mix*Veff_old + (1-mix)*Veff_new
            H = T+Veff
    """
    # Hartree-Fock:
    T = kinetic_hamiltonian(x)
    Vext = ext_potential(x)
    Vhartree=hartree_potential(ns,x)
    Veff=sp.diags(Vext+Vhartree,0)
    H=T+Veff
    
    for i in range(maxiters):
        print('\n\nIteration #{0}'.format(i))
        orbitals=[]
        for i in range(N_e):
            print('  Calculating orbitals for electron ', i+1)
            eigs, evecs = sla.eigs(H+sp.diags(VSIC[i],0), k=N_e, which='SR')
            eigs=real(eigs)
            evecs=real(evecs)
            print('    eigenvalues', eigs)
            evecs[:,occ[i]]=normalize_orbital(evecs[:,occ[i]],x)
            orbitals.append(evecs[:,occ[i]])
        Veff_old = 1.0*Veff
        ns=density(orbitals)
        Vhartree=hartree_potential(ns,x)
        VSIC=exchange_potential(ns,x, orbitals, N_e, occ)
        Veff_new=sp.diags(Vext+Vhartree,0)
        if check_convergence(Veff_old,Veff_new,threshold):
            break
        else:
            """ Mixing the potential """
            Veff= mix*Veff_old + (1-mix)*Veff_new
            H = T+Veff
    

    print('\n\n')
    off = offdiag_potential_energy(orbitals,x)
    E_kin = diagonal_energy(T,orbitals,x)
    E_pot = diagonal_energy(sp.diags(Vext,0),orbitals,x) + off
    E_tot = E_kin + E_pot
    print('Total energy     ', E_tot)
    print('Kinetic energy   ', E_kin)
    print('Potential energy ', E_pot) 
    print('\nDensity integral ', simps(ns,x))

    # Saving the data in .txt files
    save_ns_in_ascii(ns,'density')
    save_orbitals_in_ascii(orbitals,'orbitals')
    save_misc_in_ascii(E_tot, E_kin, E_pot, x, 'misc')

    # Also creating HDF5 files.
    save_data_to_hdf5_file("results",orbitals,ns,N_e,occ,x,ee_coef)

    plot(x,abs(ns), linewidth=4)
    xlabel(r'$x$ (a.u.)')
    ylabel(r'$n(x)$ (1/a.u.)')
    title('N-electron density for N={0}'.format(N_e))
    energy_text = "\n".join((
            r'Total energy:       %.2f' % (E_tot, ),
            r'Kinetic energy:    %.2f' % (E_kin, ),
            r'Potential energy: %.2f' % (E_pot, )))
    text(x[0], 0.8*max(abs(ns)), energy_text, fontsize=12)
    show()

if __name__=="__main__":
    main()
