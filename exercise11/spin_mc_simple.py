"""
Simple Monte Carlo for Ising model

Related to course FYS-4096 Computational Physics

Problem 1:
- Make the code to work, that is, include code to where it reads "# ADD"
- Comment the parts with "# ADD" and make any additional comments you 
  think could be useful for yourself later.
- Follow the assignment from ex11.pdf.

Problem 2:
- Add observables: heat capacity, magnetization, magnetic susceptibility
- Follow the assignment from ex11.pdf.

Problem 3:
- Look at the temperature effects and locate the phase transition temperature.
- Follow the assignment from ex11.pdf.

"""


from numpy import *
from matplotlib.pyplot import *
from scipy.special import erf
import h5py

class Walker:
    def __init__(self,*args,**kwargs):
        self.spin = kwargs['spin']
        self.nearest_neighbors = kwargs['nn']
        self.sys_dim = kwargs['dim']
        self.coords = kwargs['coords']

    def w_copy(self):
        return Walker(spin=self.spin.copy(),
                      nn=self.nearest_neighbors.copy(),
                      dim=self.sys_dim,
                      coords=self.coords.copy())
    

def Energy(Walkers):
    E = 0.0
    J = 4.0 # given in units of k_B
    # calculation of energy
    
    for walker in Walkers:
        E += 1/2*site_Energy(Walkers, walker)
    
    return E

def Magnetization(Walkers):
    M = 0
    for walker in Walkers:
        M += 1/2*walker.spin
        
    return M

def Heat_Capasity(Walkers, T, N):
     
    E = zeros(N)
    i = 0
    for walker in Walkers:
        E[i] = site_Energy(Walkers, walker)
        i += 1
        
    return var(E)/T
     
def Magnetic_Susceptibility(Walkers, T):
    
    spins = zeros(len(Walkers))
    for i in range(len(Walkers)):
        spins[i] = Walkers[i].spin
        
    return var(spins)/T

def site_Energy(Walkers,Walker):
    E = 0.0
    J = 4.0 # given in units of k_B
    for k in range(len(Walker.nearest_neighbors)):
        j = Walker.nearest_neighbors[k]
        E += -J*Walker.spin*Walkers[j].spin
    return E

def ising(Nblocks,Niters,Walkers,beta,T):
    N = len(Walkers)
    M = zeros((Nblocks,))
    C = zeros((Nblocks,))
    MS = zeros((Nblocks,))
    Eb = zeros((Nblocks,))
    Accept=zeros((Nblocks,))
    AccCount=zeros((Nblocks,))

    obs_interval = 5
    for i in range(Nblocks):
        EbCount = 0
        MCount = 0
        CCount = 0
        MSCount = 0
        for j in range(Niters):
            # Choosing a random site.
            site = int(random.rand()*N)

            # Saving the current values.
            s_old = 1.0*Walkers[site].spin

            E_old = site_Energy(Walkers,Walkers[site])

            # selection of new spin to variable s_new
            s_new = -1*s_old

            Walkers[site].spin = 1.0*s_new

            E_new = site_Energy(Walkers,Walkers[site])

            # Metropolis Monte Carlo
            AccCount[i] += 1
            if exp(-beta*(E_new - E_old)) < random.rand():
                # Not accepted, reverting to old values
                Walkers[site].spin = s_old
            else:
                # Accepted, iterating the counter of times accepted.
                Accept[i] += 1

            if j % obs_interval == 0:
                E_tot = Energy(Walkers)/N # energy per spin
                Eb[i] += E_tot
                M[i] += Magnetization(Walkers)/N
                C[i] += Heat_Capasity(Walkers, T, N)/N
                MS[i] += Magnetic_Susceptibility(Walkers, T)/N
                EbCount += 1
                MCount += 1
                CCount += 1
                MSCount += 1
        
        # Avaqraging the observables.
        M[i] /= MCount  
        C[i] /= CCount
        MS[i] /= MSCount
        Eb[i] /= EbCount
        Accept[i] /= AccCount[i]
        print('Block {0}/{1}'.format(i+1,Nblocks))
        print('    E   = {0:.5f}'.format(Eb[i]))
        print('    Acc = {0:.5f}'.format(Accept[i]))


    return Walkers, Eb, Accept, C, M, MS

def save_data_to_hdf5_file(Eb, C, M, MS):
    # Function to save the data in HDF5 format.
    f = h5py.File("Data","w")
    
    oset = f.create_dataset("Energy",data=Eb,dtype='f')
    oset.attrs["info"] = 'Energies'
    
    gset = f.create_dataset("C",data=C,dtype='f')
    gset.attrs["info"] = 'Heat capacity'

    dset = f.create_dataset("M", data=M, dtype='f')
    dset.attrs["info"] = 'Magnetization'
    
    nset = f.create_dataset("MS", data=MS, dtype='f')
    nset.attrs["info"] = 'Magnetic susceptibility'
    
    f.close()
    return

def main():
    Walkers=[]

    dim = 2
    grid_side = 10
    grid_size = grid_side**dim
    
    # Ising model nearest neighbors only
    mapping = zeros((grid_side,grid_side),dtype=int) # mapping
    inv_map = [] # inverse mapping
    ii = 0
    for i in range(grid_side):
        for j in range(grid_side):
            mapping[i,j]=ii
            inv_map.append([i,j])
            ii += 1
 

    # Defining the walkers for each coordinate point and also their
    # nearest neghbours. Starting state has all spins as 1/2 up.
    for i in range(grid_side):
        for j in range(grid_side):
            j1=mapping[i,(j-1) % grid_side]
            j2=mapping[i,(j+1) % grid_side]
            i1=mapping[(i-1) % grid_side,j]
            i2=mapping[(i+1) % grid_side,j]
            Walkers.append(Walker(spin=0.5,
                                  nn=[j1,j2,i1,i2],
                                  dim=dim,
                                  coords = [i,j]))
 
    
    Nblocks = 200
    Niters = 1000
    eq = 20 # equilibration "time"
    T = 3.0 # Temperature in K
    beta = 1.0/T # exchange constant
    """
    Notice: Energy is measured in units of k_B, which is why
            beta = 1/T instead of 1/(k_B T)
    """
    Walkers, Eb, Acc, C, M, MS = ising(Nblocks,Niters,Walkers,beta,T)

    fig1 = figure()
    subplot(221)
    plot(Eb)
    title("Energy")
    subplot(222)
    plot(M)
    title("Magnetization")
    subplot(223)
    plot(C)
    title("Heat capasity")
    subplot(224)
    plot(MS)
    title("Magnetic susceptibility")
    
    Eb = Eb[eq:]
    print('Ising total energy: {0:.5f} +/- {1:0.5f}'.format(mean(Eb), std(Eb)/sqrt(len(Eb))))
    print('Variance to energy ratio: {0:.5f}'.format(abs(var(Eb)/mean(Eb)))) 
    print('Magnetization: {0:.5f}'.format(mean(M)))
    print('Heat capasity: {0:.5f}'.format(mean(C)))
    print('Magnetic susceptibility: {0:.5f}'.format(mean(MS)))

    # Saving the data in HDF5 format.
    save_data_to_hdf5_file(Eb, C, M, MS)
    
    # Calculating the functions of temperature.
    T_space = linspace(1, 6, 20)
    beta_space = 1.0/T_space
    
    E_t = zeros(len(T_space))
    C_t = zeros(len(T_space))
    M_t = zeros(len(T_space))
    MS_t = zeros(len(T_space))
    for i in range(len(T_space)):
        Walkers, Eb, Acc, C, M, MS = ising(50,1000,Walkers,beta_space[i],T_space[i])
        E_t[i] = mean(Eb)
        C_t[i] = mean(C)
        M_t[i] = mean(M)
        MS_t[i] = mean(MS)
    
    fig = figure()
    subplot(221)
    plot(T_space, E_t)
    xlabel("Temperature (K)")
    title("Energy")
    subplot(222)
    plot(T_space, C_t)
    C_max = max(C_t)
    C_list = C_t.tolist()
    C_index = C_list.index(C_max)
    plot([T_space[C_index], T_space[C_index]], [0, C_max], '--')
    xlabel("Temperature (K)")
    title("Heat capasity")
    subplot(223)
    plot(T_space, M_t)
    xlabel("Temperature (K)")
    title("Magnetization")
    subplot(224)
    plot(T_space, MS_t)
    MS_max = max(MS_t)
    MS_list = MS_t.tolist()
    MS_index = MS_list.index(MS_max)
    plot([T_space[MS_index], T_space[MS_index]], [0, MS_max], '--')
    xlabel("Temperature (K)")
    title("Magnetic susceptibility")
    
    print("Phase change temperature:")
    print(mean([T_space[C_index], T_space[MS_index]]), " K")
    
    show()
if __name__=="__main__":
    main()
        
