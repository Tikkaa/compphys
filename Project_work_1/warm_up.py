"""
Project work 1 warm up problem.
"""

import numpy as np
from scipy.integrate import simps
from matplotlib.pyplot import contourf
from matplotlib.pyplot import plot

def main():
    
    def fun(co): return (co[0] + co[1])*np.exp(-0.5*np.linalg.norm(co))
    
    # Defining the lattice vectors.
    a1 = np.array([1.2, 0])
    a2 = np.array([0.6, 1])
    
    # The matrix of the coordinate transformation and its determinant, which is
    # the Jacobian to fix the integral.
    mat = np.transpose(np.matrix([a1, a2]))
    det = np.linalg.det(mat)
    
    # Points in the transformed plane.
    n = 2000
    x = np.linspace(0, 1, n)
    y = np.linspace(0, 1, n)
    values = np.zeros((n,n))
    

    # Calculating the values in the different points.
    for i in range(n):
        for j in range(n):
            values[i, j] = fun(mat*[[x[i]], [y[j]]])

    # Integrating the values in the transformed coordinates and multiplying
    # by the determinant.
    print("Integral value:")
    print(simps(simps(values, dx = 1/n), dx = 1/n)*det)
    # The value approaches the correct one, at n = 10000 it was 
    # 0.9255278976621361, but the calculation of the values takes a long time.
    
    # Calculating more values for plotting.
    x = np.linspace(-2, 3, 100)
    y = np.linspace(-2, 3, 100)
    
    # Plotting the function and the integrated area.
    print("Integrated function as a contour plot and the integrated area in red outlines")
    X, Y = np.meshgrid(x, y)
    plot_values = np.zeros((len(x), len(y)))
    for i in range(len(x)):
        for j in range(len(y)):
            plot_values[i, j] = fun([x[i], y[j]])
    
    contourf(X, Y, plot_values)
    plot([0,1.2,1.8,0.6,0], [0,0,1,1,0], 'r')
    
main()