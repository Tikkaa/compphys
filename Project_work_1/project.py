"""
Project 1
Problem 1: Induced magnetic field by current loops
Functions to calculate magnetic fields caused by wire loops.
"""

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d

def differential_magnetic_field(I, dl, r):
    # Calcualtes the magnetic field caused by current I in piece of wire
    # defined by vector dl at a point vector r away from the wire.
    
    dB = 1e-7*I*np.cross(dl, r*1/np.linalg.norm(r))/np.linalg.norm(r)**2
    return dB


def wire_loop_vectors(radius, n):
    # Defines a wire loop in the yz plane as a series of n vectors and
    # coordinates.
    
    point_angles = np.linspace(0, 2*np.pi, n+1)
    
    # Array for the points in yz plane.
    points = np.zeros((n+1,3))
    
    # Calculating the points.
    for i in range(n+1):
        points[i,1] = radius*np.cos(point_angles[i])
        points[i,2] = radius*np.sin(point_angles[i])
    
    # Array containing the vectors and their starring points.
    vectors = np.zeros((n,6))
    
    # Calculating the vectors and putting the in an array with the points.
    for i in range(n):
        vectors[i,0:3] = points[i+1]-points[i]
        vectors[i,3:7] = points[i]
    
    return vectors

def magnetic_field(co, loop, I):
    # Calculates the magnetic field in coordinates co using array 'loop' in the
    # format given by wire_loop_vectors().
    
    n = loop.shape[0]
    field = np.zeros(3)
    
    for i in range(n):
        field += differential_magnetic_field(I, loop[i, 0:3], co - loop[i, 3:7])
    
    return field


def test_x_axis():
    # Tests the magnetic_field function at points along the x-axis by comparing
    # the results with analytical solutions. Returns True if the results are
    # close enough and False otherwise.
    
    I = 100
    r = 1
    
    # Analytical answer:
    def ana(x): return 4*np.pi*1e-7*I*r**2/(2*(x**2 + r**2)**(3/2))
    
    # Creating the loop.
    loop = wire_loop_vectors(r, 1000)
    
    
    # Testing the magnetic_field() against the analytical answers.
    x = np.linspace(-10,10,20)
    for i in x:
        if np.abs(magnetic_field([i, 0, 0], loop, I)[0] - ana(i)) > 1e-10:
            return False
        else:
            return True

def main():
    # Tests and a graph to demonstarte the functions.
    
    # Running the test function.
    if not test_x_axis():
        print("magnetic_field() is not working properly.")
    
    I = 1
    r = 1
    
    # Defining two wire loops.
    loop1 = wire_loop_vectors(r, 100)
    loop2 = 1*loop1
    loop1[:,3] = -2*r
    loop2[:,3] = 2*r
    
    # Defining points for graphing.
    n = 7
    x = np.linspace(-5*r,5*r,n)
    y = np.linspace(-5*r,5*r,n)
    z = np.linspace(-5*r,5*r,n)
    
    X, Y, Z = np.meshgrid(x, y, z)
    u = np.zeros((n,n,n))
    v = np.zeros((n,n,n))
    w = np.zeros((n,n,n))
    
    # Calculating the field values in the points.
    for i in range(n):
        for j in range(n):
            for k in range(n):
                field = magnetic_field([x[i],y[j],z[k]], loop1, I) + magnetic_field([x[i],y[j],z[k]], loop2, I)
                u[i,j,k] = field[0]
                v[i,j,k] = field[1]
                w[i,j,k] = field[2]
    
    # Plotting the vector field and the wire loops.
    # Vector lengths normalized to be the same to show the shape of the field.
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    # For some reason the x and y coodrinates need to be switched here in some
    # parts. No idea why. Otherwise the field will not be symmetrical.
    ax.quiver(X, Y, Z, v, u, w, lw = 2, normalize = True)
    ax.set_title("Lengths normalized to the same value")
    ax.plot(loop1[:,4],loop1[:,3],loop1[:,5],'red')
    ax.plot(loop2[:,4],loop2[:,3],loop2[:,5],'red')
    plt.show()
    
    # Vector lentths normalized to show magnitudes.
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.quiver(X, Y, Z, v, u, w, lw = 2, length = 1e7)
    ax.set_title("Vector length multiplied by 1e7")
    ax.plot(loop1[:,4],loop1[:,3],loop1[:,5],'red')
    ax.plot(loop2[:,4],loop2[:,3],loop2[:,5],'red')
    plt.show()

main()