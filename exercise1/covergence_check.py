"""Functions that plot the absolute error of numerical estimates of first and
second derivatives and integration using trapezoid rule, as a function of spacing."""

import numpy as np
import matplotlib.pyplot as plt
from num_calculus import first_derivative
from num_calculus import second_derivative
from num_calculus import trapezoid_rule

def derivative_error(fun, test_fun, d_test_fun, x, diffs):
    # A function that calculates the absolute error of numerical estimate of first
    # or second derivative as a function of spacing and returns them in a list.

    errors = []
    for i in range(len(diffs)):
        errors.append(abs(fun(test_fun, x, diffs[i]) - d_test_fun(x)))

    return errors


def trapezoid_error(test_fun, i_test_fun):
    # A function that calculates the absolute error of numerical estimate of
    # integral as a function of spacing and returns them in a list.

    errors = []
    x = np.linspace(2,100,98)
    for i in range(len(x)):
        errors.append(abs(trapezoid_rule(np.linspace(0,1,int(x[i])), test_fun)-i_test_fun))
    return errors

def plot_errors():
    # A function that makes three figures showing the absolute error of first and second
    # derivative and integration using trapezoid rule, as a function of spacing.

    # Defining the test function as well as its first and second derivative and integral
    # from 0 to 1.
    def test_fun(x): return np.sin(x)
    def d_test_fun(x): return np.cos(x)
    def dd_test_fun(x): return -1*np.sin(x)
    i_test_fun = -1*np.cos(1) + 1
    diffs = np.linspace(0.00001, 1, 10000)
    first_errors = derivative_error(first_derivative, test_fun, d_test_fun, 1, diffs)
    second_errors = derivative_error(second_derivative, test_fun, dd_test_fun, 1, diffs)
    trapezoid_errors = trapezoid_error(test_fun, i_test_fun)

    # Drawing the three figures:
    fig = plt.figure()
    ax = fig.add_subplot(111)

    ax.plot(diffs, first_errors)
    ax.set_title("First derivative")
    ax.set_xlabel("h")
    ax.set_ylabel("absolute error")
    plt.show()

    fig2 = plt.figure()
    ax = fig2.add_subplot(111)

    ax.plot(diffs, second_errors)
    ax.set_title("Second derivative")
    ax.set_xlabel("h")
    ax.set_ylabel("absolute error")
    plt.show()

    fig3 = plt.figure()
    ax = fig3.add_subplot(111)

    ax.plot(np.linspace(2,100,98), trapezoid_errors)
    ax.set_title("Integral using trapezoid rule")
    ax.set_xlabel("number of intervals")
    ax.set_ylabel("absolute error")
    plt.show()


def main():
    plot_errors()


main()